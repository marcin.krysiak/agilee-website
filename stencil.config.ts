import { Config } from "@stencil/core";
import nodePolyfills from "rollup-plugin-node-polyfills";
import { sass } from "@stencil/sass";

// https://stenciljs.com/docs/config

export const config: Config = {
  namespace: "agilee-io-website",
  minifyJs: true,
  minifyCss: true,
  copy: [
    { src: "submitForm.php" },
    { src: "PHPMailer.php" },
    { src: ".htaccess" },
    { src: "robots.txt" },
    { src: "sitemap.xml" },
  ],
  outputTargets: [
    {
      type: "www",
      dir: "www",
      empty: true,
      // comment the following line to disable service workers in production
      serviceWorker: {
        globPatterns: ["**/*.{js,css,json,html,ico,png,svg,mp4,ttf}"],
      },
      baseUrl: "https://agilee.io",
    },
  ],
  plugins: [nodePolyfills(), sass()],
};
