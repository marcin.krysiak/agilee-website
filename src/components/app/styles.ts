import { Styles } from "jss";

export default <Styles>{
  content: {
    display: "flex",
    alignItems: "center",
    overflowX: "hidden",
    overflowY: "auto",
    flexDirection: "column",
    width: "100%",
  },
  appContent: {
    backgroundColor: "white",
    height: 2000,
  },
};
