import { Component, h } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-logo-link",
})
export class LogoLink {
  render() {
    return (
      <a href="#">
        {" "}
        <img
          src="assets/images/full-logo.svg"
          alt="Agilee IO full logo"
          class={classes.logo}
        />
      </a>
    );
  }
}
