import colors from "../../theme/colors";
import { Styles } from "jss";
import animations from "../../theme/animations";

export default <Styles>{
  ...animations.spinner,
  container: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    position: "relative",
    width: "100%",
    minHeight: "80vh",
    padding: "2rem",
    backgroundColor: colors.white,
    perspective: 1000,
    // background: `radial-gradient(circle, ${colors.white} 50%, ${colors.primary} 100%)`,
  },
  card: {
    padding: 25,
    boxSizing: "border-box",

    "&.rotate": {
      transition: "0.6s",
      transformStyle: "preserve-3d",
      transform: "rotateY(180deg)",
    },

    "@media (max-width: 1024px)": {
      padding: 0,
      maxWidth: 330,
      boxShadow: "none",
      border: "none",
    },
  },
  form: {
    "--mdc-theme-primary": colors.primary,
    display: "contents",
    textAlign: "center",
    width: "100%",
    maxWidth: 1600,
    color: colors.black,
    backfaceVisibility: "hidden",

    "& .mdc-text-field--focused:not(.mdc-text-field--disabled):not(.mdc-text-field--invalid) .mdc-floating-label": {
      color: colors.primary,
    },

    "& .mdc-button--outlined:not(:disabled)": {
      borderColor: colors.primary,
    },
  },
  textField: {
    margin: "10px 0",
    minWidth: 400,

    "@media (max-width: 1024px)": {
      minWidth: 330,
    },
  },
  fileInput: {
    visibility: "hidden",
  },
  button: {
    "@media (min-width: 1024px)": {
      alignSelf: "flex-end",
    },
  },
  cardBack: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    backgroundColor: colors.primary,
    color: colors.black,
    borderRadius: 4,
    boxSizing: "border-box",
    padding: 30,
    transform: "rotateY(-180deg)",
    backfaceVisibility: "hidden",
    transformStyle: "preserve-3d",

    "& img": {
      backfaceVisibility: "hidden",
      width: 100,
      animation: "$spinner 1s linear infinite",
    },
  },
};
