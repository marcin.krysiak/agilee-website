import { Component, Host, h, Element } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-portfolio",
})
export class Portfolio {
  @Element() el: HTMLElement;

  io: IntersectionObserver;

  componentDidLoad() {
    this.addIntersectionObserver();
  }

  addIntersectionObserver() {
    this.io = new IntersectionObserver((data: any) => {
      data.forEach(element => {
        element.isIntersecting
          ? element.target.classList.add("in-view")
          : element.target.classList.remove("in-view");
      });
    });

    this.el
      .querySelectorAll(".animate-in-view")
      .forEach(element => this.io.observe(element));
  }

  render() {
    return (
      <Host class={classes.container}>
        <div class={classes.content}>
          <h1>Portfolio</h1>
          <div class={classes.mockups}>
            <div class={`animate-in-view ${classes.mask} ${classes.mobile}`}>
              <img
                src="assets/portfolio/s12.png"
                alt="S12 Solutions portfolio"
              />
              <div class={classes.cardOverlay}>
                <p>React Native app</p>
                <p>Booking app for health sector</p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.mobile}`}>
              <img src="assets/portfolio/coty.png" alt="Coty portfolio" />
              <div class={classes.cardOverlay}>
                <p>Angular Ionic mobile app</p>
                <p>Intranet news browser and messenger for fashion leader</p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.laptop}`}>
              <img
                src="assets/portfolio/j&j.png"
                alt="Johnson&Johnson portfolio"
              />
              <div class={classes.cardOverlay}>
                <p>ExtJS desktop web app</p>
                <p>
                  Research Projects Management System for a leading player in
                  cosmetic sector
                </p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.desktop}`}>
              <img
                src="assets/portfolio/barclaycard.png"
                alt="Barclaycard portfolio"
              />
              <div class={classes.cardOverlay}>
                <p>React responsive web app</p>
                <p>
                  Acquisition responsive web application for credit cards and
                  loans - finance sector
                </p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.tablet}`}>
              <img
                src="assets/portfolio/specsavers.png"
                alt="Specsavers portfolio"
              />
              <div class={classes.cardOverlay}>
                <p>Angular Ionic PWA app</p>
                <p>
                  E-commerce responsive app for leading glasses and contact
                  lenses retailer
                </p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.mobile}`}>
              <img src="assets/portfolio/bpay.png" alt="bPay portfolio" />
              <div class={classes.cardOverlay}>
                <p>React Native mobile and web app</p>
                <p>
                  Management dashboard for wearable wireless payment devices -
                  finance sector
                </p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.mobile}`}>
              <img
                src="assets/portfolio/trainline.png"
                alt="Trainline portfolio"
              />
              <div class={classes.cardOverlay}>
                <p>React web and mobile app</p>
                <p>
                  Train tickets e-commerce platform for leading company in
                  transport sector
                </p>
              </div>
            </div>
            <div class={`animate-in-view ${classes.mask} ${classes.desktop}`}>
              <div>And much more...</div>
              <div class={classes.cardOverlay}>
                <p />
                <p>
                  We delivered a number of client and server-side applications
                  following the best security and development standards. Helped
                  our clients with design, architecture, prototype, develop,
                  project management, testing, and production deployment.
                </p>
              </div>
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
