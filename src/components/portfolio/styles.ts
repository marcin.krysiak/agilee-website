import colors from "../../theme/colors";
import { Styles } from "jss";

export default <Styles>{
  container: {
    display: "flex",
    justifyContent: "center",
    position: "relative",
    width: "100%",
    minHeight: "95vh",
    backgroundColor: colors.primary,
    padding: "2rem",
  },
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    maxWidth: 1600,
    color: colors.black,
  },
  mockups: {
    display: "grid",
    gridGap: 50,
    gridTemplateColumns: "repeat(5, 1fr)",
    gridAutoFlow: "row dense",
    gridAutoRows: "minmax(100px, auto)",
    justifyItems: "center",
    alignItems: "center",
    width: "90%",
    height: "100%",
    marginTop: 20,

    "& img, & div": {
      width: "100%",
      height: "100%",
      objectFit: "contain",
      zIndex: 10,
    },

    "@media (max-width: 1024px)": {
      gridTemplateColumns: "repeat(3, 1fr)",
      gridGap: 10,
    },
  },
  mobile: {
    gridColumn: "span 1",
    gridRow: "span 2",
    padding: 30,

    "@media (max-width: 1024px)": {
      padding: 10,
    },
  },
  tablet: {
    gridColumn: "span 2",
    gridRow: "span 2",
    padding: 70,

    "@media (max-width: 1024px)": {
      padding: 10,
    },
  },
  laptop: {
    gridColumn: "span 3",
    gridRow: "span 2",
    padding: 70,

    "@media (max-width: 1024px)": {
      padding: 10,
    },
  },
  desktop: {
    gridColumn: "span 3",
    gridRow: "span 2",
    padding: 70,

    "@media (max-width: 1024px)": {
      padding: 10,
    },
  },

  mask: {
    display: "flex",
    position: "relative",
    justifyItems: "center",
    alignItems: "center",
    boxSizing: "border-box",
    height: "100%",
    transform: "translateY(100px)",
    opacity: 0,
    overflow: "hidden",

    "&.in-view": {
      animationFillMode: "forwards",
      transform: "translateY(0)",
      opacity: 1,
      transition: "all 1s cubic-bezier(0.23, 1, 0.32, 1) 0.2s",
    },

    "&:hover": {
      "& img": {
        transform: "scale(1.1)",
        transition: "all 3s cubic-bezier(0.23, 1, 0.32, 1)",
      },
    },
  },

  cardOverlay: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: colors.black,
    color: colors.white,
    boxSizing: "border-box",
    borderRadius: 10,
    padding: 30,
    zIndex: 20,
    opacity: 0,
    transition: "all 0.6s ease-in-out",

    "@media (max-width: 1024px)": {
      padding: 10,
    },

    "&:hover": {
      opacity: 0.95,
    },

    "& p": {
      margin: "0 0 10px",
      lineHeight: 1.2,

      "&:nth-of-type(1)": {
        fontSize: 14,
        color: "#999",

        "@media (max-width: 1024px)": {
          fontSize: 12,
        },
      },
      "&:nth-of-type(2)": {
        position: "relative",
        fontSize: 20,

        "@media (max-width: 1024px)": {
          fontSize: 14,
        },

        "&:after": {
          background: "none repeat scroll 0 0 #666",
          bottom: -15,
          content: '""',
          height: 1,
          left: 1,
          position: "absolute",
          width: 40,
          transition: "all 0.2s ease-in-out",
        },
      },
    },
  },
};
