import colors from "../../theme/colors";
import { Styles } from "jss";

export default <Styles>{
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    width: "100%",
    minHeight: "80vh",
    backgroundColor: colors.black,
    padding: "2rem",
  },
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    maxWidth: 1600,
    color: colors.white,
  },
  services: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    marginTop: 20,
    width: "100%",
  },
  service: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    width: 300,
    padding: "20px 5%",
  },
  serviceImage: {
    width: "auto",
    height: "5rem",
  },
  technologies: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    width: "100%",
    marginTop: 20,
    opacity: 0.05,

    "& img": {
      height: "5vw",
      maxHeight: 50,
    },
  },
};
