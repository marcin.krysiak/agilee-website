import { Component, Host, h } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-clients",
})
export class Clients {
  render() {
    return (
      <Host class={classes.container}>
        <div class={classes.content}>
          <h1>Trusted us</h1>
          <div class={classes.logos}>
            <img src="assets/clients/coty.png" alt="Coty logo" />
            <img src="assets/clients/bt.svg" alt="British Telecom logo" />
            <img src="assets/clients/barclays.svg" alt="Barclays Bank logo" />
            <img src="assets/clients/barclaycard.svg" alt="Barclaycard logo" />
            <img src="assets/clients/specsavers.svg" alt="Specsavers logo" />
            <img src="assets/clients/trainline.svg" alt="Trainline logo" />
            <img src="assets/clients/j&j.svg" alt="Johnson&Johnson logo" />
            <img
              src="assets/clients/s12-solutions.png"
              alt="S12 Solutions logo"
            />
          </div>
        </div>
      </Host>
    );
  }
}
