import animations from "../../theme/animations";
import { Styles } from "jss";

export default <Styles>{
  ...animations.fadeIn,
  ...animations.bounce,
  "@keyframes movingLogo": {
    "0%": {
      opacity: 0,
    },
    "10%": {
      opacity: 1,
    },
    "90%": {
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
    },
    "100%": {
      top: 30,
      left: 30,
      width: 150,
      height: 25,
      opacity: 1,
    },
  },
  "@keyframes scalingLogo": {
    "90%": {
      transform: "scale(1)",
    },
    "100%": {
      transform: "scale(0.5)",
    },
  },
  container: {
    display: "flex",
    justifyContent: "center",
    position: "relative",
    width: "100%",
    height: "95vh",
    overflow: "hidden",
  },
  video: {
    position: "absolute",
    objectFit: "cover",
  },
  colorMask: {
    position: "absolute",
    width: "100%",
    height: "100%",
    // backgroundColor: colors.primary,
    backgroundColor: "#11F3dd", // to match the primary color with background, opacity and overlay
    opacity: 0.85,
  },
  heroContent: {
    position: "absolute",
    maxWidth: 1600,
    width: "100%",
    height: "100%",
  },
  logo: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    opacity: 0,
    width: "100%",
    height: "100%",
    animationDelay: "1s",
    animationFillMode: "forwards",
    animation: "$movingLogo 5s ease, $bounce 2s",

    "& app-logo-link": {
      animationDelay: "1s",
      animationFillMode: "forwards",
      animation: "$scalingLogo 5s ease",
    },
  },
  header: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    verticalAlign: "middle",
    position: "absolute",
    width: "100%",
    height: "100%",
    padding: 50,
    opacity: 0,
    boxSizing: "border-box",
    textAlign: "center",
    flexWrap: "wrap",
    animationDelay: "6.5s",
    animationFillMode: "forwards",
    animation: "$fadeIn 1s ease, $bounce 2s",
  },
  contactUs: {
    position: "absolute",
    textAlign: "right",
    top: 24,
    right: 20,
    opacity: 0,
    boxSizing: "border-box",
    animationDelay: "10s",
    animationFillMode: "forwards",
    animation: "$fadeIn 1s ease, $bounce 2s",
  },
};
