/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';


export namespace Components {
  interface AppClients {}
  interface AppContact {}
  interface AppContactLink {}
  interface AppHero {
    'image': string;
  }
  interface AppHome {}
  interface AppLogoLink {}
  interface AppNav {
    'image': string;
  }
  interface AppPortfolio {}
  interface AppServices {}
}

declare global {


  interface HTMLAppClientsElement extends Components.AppClients, HTMLStencilElement {}
  var HTMLAppClientsElement: {
    prototype: HTMLAppClientsElement;
    new (): HTMLAppClientsElement;
  };

  interface HTMLAppContactElement extends Components.AppContact, HTMLStencilElement {}
  var HTMLAppContactElement: {
    prototype: HTMLAppContactElement;
    new (): HTMLAppContactElement;
  };

  interface HTMLAppContactLinkElement extends Components.AppContactLink, HTMLStencilElement {}
  var HTMLAppContactLinkElement: {
    prototype: HTMLAppContactLinkElement;
    new (): HTMLAppContactLinkElement;
  };

  interface HTMLAppHeroElement extends Components.AppHero, HTMLStencilElement {}
  var HTMLAppHeroElement: {
    prototype: HTMLAppHeroElement;
    new (): HTMLAppHeroElement;
  };

  interface HTMLAppHomeElement extends Components.AppHome, HTMLStencilElement {}
  var HTMLAppHomeElement: {
    prototype: HTMLAppHomeElement;
    new (): HTMLAppHomeElement;
  };

  interface HTMLAppLogoLinkElement extends Components.AppLogoLink, HTMLStencilElement {}
  var HTMLAppLogoLinkElement: {
    prototype: HTMLAppLogoLinkElement;
    new (): HTMLAppLogoLinkElement;
  };

  interface HTMLAppNavElement extends Components.AppNav, HTMLStencilElement {}
  var HTMLAppNavElement: {
    prototype: HTMLAppNavElement;
    new (): HTMLAppNavElement;
  };

  interface HTMLAppPortfolioElement extends Components.AppPortfolio, HTMLStencilElement {}
  var HTMLAppPortfolioElement: {
    prototype: HTMLAppPortfolioElement;
    new (): HTMLAppPortfolioElement;
  };

  interface HTMLAppServicesElement extends Components.AppServices, HTMLStencilElement {}
  var HTMLAppServicesElement: {
    prototype: HTMLAppServicesElement;
    new (): HTMLAppServicesElement;
  };
  interface HTMLElementTagNameMap {
    'app-clients': HTMLAppClientsElement;
    'app-contact': HTMLAppContactElement;
    'app-contact-link': HTMLAppContactLinkElement;
    'app-hero': HTMLAppHeroElement;
    'app-home': HTMLAppHomeElement;
    'app-logo-link': HTMLAppLogoLinkElement;
    'app-nav': HTMLAppNavElement;
    'app-portfolio': HTMLAppPortfolioElement;
    'app-services': HTMLAppServicesElement;
  }
}

declare namespace LocalJSX {
  interface AppClients {}
  interface AppContact {}
  interface AppContactLink {}
  interface AppHero {
    'image'?: string;
  }
  interface AppHome {}
  interface AppLogoLink {}
  interface AppNav {
    'image'?: string;
  }
  interface AppPortfolio {}
  interface AppServices {}

  interface IntrinsicElements {
    'app-clients': AppClients;
    'app-contact': AppContact;
    'app-contact-link': AppContactLink;
    'app-hero': AppHero;
    'app-home': AppHome;
    'app-logo-link': AppLogoLink;
    'app-nav': AppNav;
    'app-portfolio': AppPortfolio;
    'app-services': AppServices;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements {
      'app-clients': LocalJSX.AppClients & JSXBase.HTMLAttributes<HTMLAppClientsElement>;
      'app-contact': LocalJSX.AppContact & JSXBase.HTMLAttributes<HTMLAppContactElement>;
      'app-contact-link': LocalJSX.AppContactLink & JSXBase.HTMLAttributes<HTMLAppContactLinkElement>;
      'app-hero': LocalJSX.AppHero & JSXBase.HTMLAttributes<HTMLAppHeroElement>;
      'app-home': LocalJSX.AppHome & JSXBase.HTMLAttributes<HTMLAppHomeElement>;
      'app-logo-link': LocalJSX.AppLogoLink & JSXBase.HTMLAttributes<HTMLAppLogoLinkElement>;
      'app-nav': LocalJSX.AppNav & JSXBase.HTMLAttributes<HTMLAppNavElement>;
      'app-portfolio': LocalJSX.AppPortfolio & JSXBase.HTMLAttributes<HTMLAppPortfolioElement>;
      'app-services': LocalJSX.AppServices & JSXBase.HTMLAttributes<HTMLAppServicesElement>;
    }
  }
}


